import axios from "axios";

export const GET_DATA = "GET_DATA"
export const ADD_DATA = "ADD_DATA"
export const EDIT_DATA = "EDIT_DATA"

export const getDataThunk = () => async (dispatch) => {
  try {
    const response = await axios.get(`http://localhost:5000/data`);
    dispatch({
      type: GET_DATA,
      payload: response.data
    })
  } catch (e) {
    console.error('Error: ', e)
  }
}

export const addDataThunk = (data) => async (dispatch) => {
  try {
    const response = await axios.put(`http://localhost:5000/data`, data);
    dispatch({
      type: ADD_DATA,
      payload: response.data
    })
  } catch (e) {
    console.error('Error: ', e)
  }
}

export const editDataThunk = (data) => async (dispatch) => {
  try {
    const response = await axios.put(`http://localhost:5000/data/`, data);
  } catch (e) {
    console.error('Error: ', e)
  }
}

