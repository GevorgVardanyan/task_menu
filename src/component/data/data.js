import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Multiselect from 'multiselect-react-dropdown';

import { addDataThunk, editDataThunk, getDataThunk } from "../redux/action/dataAction";

import ingredientsList from "../../ingredients";

import css from "../data/data.module.scss"


const nameRegex = new RegExp('^(?=.*[A-Za-z0-9])[A-Za-z0-9, .!@#$%^&*()_]{6,25}$');
const types = ['Drink', 'Food', 'Snack'];

const Data = () => {
  const dispatch = useDispatch();
  const { data } = useSelector(state => state.data);

  const [addNewItem, setAddNewItem] = useState(false);
  const [name, setName] = useState("");
  const [type, setType] = useState(types[0]);
  const [ingredients, setIngredients] = useState(ingredientsList[0].name);
  const [editMode, setEditMode] = useState(false);
  const [updateData, setupdateData] = useState(data);
  const [errors, setErrors] = useState({});

  const add = () => {
    data[name] = { name, type, ingredients, id: Object.keys(data).length + 1 }
    dispatch(addDataThunk(data)).then(_ => dispatch(getDataThunk())).then(_ => {
      setAddNewItem(false);
      setName("");
      setType("");
      setIngredients("");
    })
  }

  const save = () => {
    setEditMode(false);
    dispatch(editDataThunk(updateData));
  }

  const edit = (id, name, value) => {
    let newData = { ...data };
    let toBeUpdated = Object.values(newData).find(el => el.id === id);

    if (name === 'ingredients') {
      toBeUpdated[name] = [...(new Set([toBeUpdated[name], value.map(e => e.name)]))];
    } else {
      toBeUpdated[name] = value;
    }
    setupdateData(newData);
  }

  const addName = (value) => {
    let isNameValid = nameRegex.test(value);
    if (!isNameValid) {
      setErrors({ ...errors, 'name': 'Please enter valid name' })
    } else {
      setErrors({ ...errors, 'name': '' })
    }
    setName(value);
  }
  useEffect(() => {
    dispatch(getDataThunk());
  }, []);

  useEffect(() => {
    setupdateData(data);
  }, [data]);

  return (
    <div className={css.menuHolder}>
      <div className={css.controlButtons}>
        <div>
          <button onClick={save} disabled={!editMode}>Save</button>
          <button className={css.editBtn} onClick={() => setEditMode(true)}>Edit</button>
        </div>
        <button
          onClick={() => {
            setAddNewItem(true)
          }}
        >Add new item
        </button>
      </div>
      {
        addNewItem &&
        <div className={css.newItemContainer}>
          <>
            <input type="text" placeholder="Name" className={css.item} onChange={(e) => {
              addName(e.target.value)
            }} />
            {errors['name'] ? <span className={css.error}>{errors['name']}</span> : ''}
          </>
          <select className={css.item} onChange={(e) => setType(e.target.value)}>
            {types.map(op =>
              <option value={op}>{op}</option>)}
          </select>
          <select className={css.item} onChange={(e) => setIngredients(e.target.value)}>
            {ingredientsList.map(op =>
              <option key={op.id} value={op.name}>{op.name}</option>)}
          </select>
          <button
            disabled={(!name || !type || !ingredients)}
            className={css.addBtn}
            onClick={add}
          >Add</button>
        </div>

      }
      <div className={css.menu}>
        <div className={css.container}>
          <span className={css.item}>Name</span>
          <span className={css.item}>Type</span>
          <span className={css.item}>Ingredients</span>
          <div/>
        </div>
        {
          Object.values(updateData).map(({ id, name, type, ingredients }, index) => {
            return <div key={index} className={css.container}>
              <input className={css.item} disabled={!editMode} value={name} onChange={(e) => {
                edit(id, 'name', e.target.value)
              }} />
              <input className={css.item} disabled={!editMode} value={type} onChange={(e) => {
                edit(id, 'type', e.target.value)
              }} />
              <input className={css.item} disabled={!editMode} value={ingredients} onChange={(e) => {
                edit(id, 'ingredients', e.target.value)
              }} />
              <Multiselect
                disable={!editMode}
                options={ingredientsList}
                // selectedValues={ingredientsList[0]}
                onSelect={(e) => edit(id, 'ingredients', e)}
                // onRemove={this.onRemove} // Function will trigger on remove event
                displayValue="name"
              />
            </div>
          })
        }
      </div>
    </div>
  );
};

export default Data;