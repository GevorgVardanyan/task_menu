
import './App.css';
import {useDispatch, useSelector} from "react-redux";
import Data from "./component/data/data";


function App() {

  return (
    <div className="App">

      <Data/>
    </div>
  );
}

export default App;
